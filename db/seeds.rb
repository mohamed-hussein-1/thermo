# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


Thermostat.create(location: "16 flower street", household_token: "31221312312")
Thermostat.create(location: "17 flower street", household_token: "31221312314")
Thermostat.create(location: "18 flower street", household_token: "31221312315")
Thermostat.create(location: "19 flower street", household_token: "31221312316")
