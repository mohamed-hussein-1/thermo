require 'connection_pool'

conf = YAML.load(File.read(Rails.root.join('config', 'redis.yml')))
redis_config = conf[Rails.env.to_s]

Redis.current = ConnectionPool.new(size: 10, timeout: 5) do
  Redis.new host: redis_config['host'], port: redis_config['port'], db: redis_config['db']
end