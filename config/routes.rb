Rails.application.routes.draw do
  get 'reading/get_reading'
  get 'reading/get_stats'
  post 'reading/create_reading'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
