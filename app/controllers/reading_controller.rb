class ReadingController < ApplicationController

  skip_before_action :verify_authenticity_token
  before_action :authenticate_thermostat
  attr_accessor :current_thermostat

  def get_reading
    tracking_number = request.query_parameters[:tracking_number]
    if tracking_number.blank?
      return render json: {message: "invalid tracking number"}, status: 404
    end
    if (reading = Reading.get_redis_reading(tracking_number, current_thermostat)).blank?
      reading = Reading.find_by(tracking_number: tracking_number, thermostat_id: @current_thermostat.id)
    end
    if reading.blank?
      return render json: {message: "invalid tracking number"}, statys: 404
    end
    render json: {temperature: reading.temperature, humidity: reading.humidity, battery_charge: reading.battery_charge}
  end

  def get_stats
    stats = {}
    Redis.current.with do |conn|
      stats = current_thermostat.calculate_stats(conn)
    end
    render json: stats
  end

  def create_reading
    cur_params = reading_params
    tracking_number = 0
    Redis.current.with do |conn|
      tracking_number = current_thermostat.add_reading(conn,cur_params)
    end
    render json: {tracking_number: tracking_number}
  end

  private

  def reading_params
    params.permit(:temperature, :humidity, :battery_charge)
  end

  # this function reads request header authorization and sets up controller variable with the appriopriate thermostat
  def authenticate_thermostat
    return if current_thermostat.present?
    household_token = request.headers["AUTHORIZATION"]
    if household_token.blank?
      return render json: {message: "authentication token not found"}, status: 401
    end
    self.current_thermostat = Thermostat.find_by(household_token: household_token)
    if self.current_thermostat.blank?
      return render json: {message: "authentication token not valid"}, status: 401
    end
  end

end
