class Thermostat < ApplicationRecord
  has_many :readings

  STATS_ATTRIBUTES = [:temperature, :humidity, :battery_charge]
  STATS = [:sort, :sum] # redis keys we hold for calculating states such as min , max and sum

  # redis key helpers

  # redis key for object holding last sequence number generated
  # used to generate an incremented sequence number for every reading
  def get_redis_sequence_key
    "tracking_number:#{id}"
  end

  # redis key for object holding number of readings
  # this is used mainly to calculate average
  def get_redis_total_readings
    "count:#{id}"
  end

  def get_redis_attribute_stat_key(stat, attribute)
    raise 'non supported attribute' unless STATS_ATTRIBUTES.include?(attribute)
    raise 'non supported stat' unless STATS.include?(stat)
    "#{attribute}:#{stat}:#{id}"
  end

  def calculate_stats(redis_connection)
    stats = {}
    count = redis_connection.get(get_redis_total_readings).to_i
    STATS_ATTRIBUTES.inject(stats) do |acc, curr|
      acc[curr] = {average: calculate_redis_average(redis_connection, curr, count),min: calculate_redis_min(redis_connection, curr), max: calculate_redis_max(redis_connection, curr)}
      acc
    end
    stats
  end

  def calculate_redis_average(redis_connection, attribute, count)
    redis_connection.get(get_redis_attribute_stat_key(:sum, attribute)).to_f / count
  end

  def calculate_redis_min(redis_connection, attribute)
    min = redis_connection.zrange(get_redis_attribute_stat_key(:sort, attribute), 0, 0)[0]
    min = min.to_f if min.present?
    min
  end

  def calculate_redis_max(redis_connection, attribute)
    max = redis_connection.zrevrange(get_redis_attribute_stat_key(:sort, attribute), 0, 0)[0]
    max = max.to_f if max.present?
    max
  end

  def construct_cached_object(params)
    STATS_ATTRIBUTES.inject({}) do |acc, curr|
      acc[curr] = params[curr] if params[curr].present?
      acc
    end
  end

  def add_reading(redis_connection, params)
    # add redis values
    tracking_number = redis_connection.incr get_redis_sequence_key
    cached_object = construct_cached_object(params)
    redis_connection.mapped_hmset(Reading.get_cached_redis_object_key(id, tracking_number), cached_object)
    redis_connection.incr(get_redis_total_readings)
    STATS_ATTRIBUTES.each do |attribute|
      redis_connection.incrbyfloat(get_redis_attribute_stat_key(:sum, attribute), params[attribute])
      redis_connection.zadd(get_redis_attribute_stat_key(:sort, attribute), [params[attribute], params[attribute].to_f])
    end
    # add job to write in db
    ReadingWorker.perform_async(
        id,
        params[:temperature].to_f,
        params[:humidity].to_f,
        params[:battery_charge].to_f,
        tracking_number
    )
    tracking_number
  end
end
