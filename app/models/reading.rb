class Reading < ApplicationRecord
  belongs_to :thermostat


  class << self
    ## returns the key of redis object that store values
    def get_cached_redis_object_key(thermostat_id, tracking_number)
      "reading_value:#{thermostat_id}:#{tracking_number}"
    end
    def get_redis_reading(tracking_number, thermostat)
      Redis.current.with do |conn|
        arr = conn.hmget(get_cached_redis_object_key(thermostat.id, tracking_number), "temperature", "humidity", "battery_charge")
        return arr.compact.blank? ? nil : Reading.new(thermostat: thermostat,
                    tracking_number: tracking_number,
                    temperature: arr[0],
                    humidity: arr[1],
                    battery_charge: arr[2])
      end
    end
  end
end
