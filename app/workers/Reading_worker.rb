class ReadingWorker
  include Sidekiq::Worker

  def perform(thermostat_id, temperature, humidity, battery_charge, tracking_number)
    Reading.find_or_create_by(tracking_number: tracking_number, thermostat_id: thermostat_id) do |reading|
      reading.thermostat_id = thermostat_id
      reading.temperature = temperature
      reading.battery_charge = battery_charge
      reading.humidity = humidity
      reading.tracking_number = tracking_number
    end
    Redis.current.with do |conn|
      conn.del(Reading.get_cached_redis_object_key(thermostat_id, tracking_number))
    end
  end
end