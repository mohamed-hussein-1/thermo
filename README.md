# Freezing

This repo contains a basic implementation for frozen challenge

Project architecture consists of an api that responds to 3 requests

in order to escape database write bottleneck, writes to db are being done in background jobs  backed by ```sidekiq```

To have a consistent api even in case background job didn't start yet objects are cached inside ```redis```
Redis is also used to generate unique sequence number for every thermostat and keep sum and min and max to calculate the required stats


To install project :-

* make sure you installed redis locally and get it running
edit the ```redis.yml``` to match your local redis configuration

* make sure to have postgres installed locally 9.5+ and edit the database.yml username and password to match yours

* run bundle install from project app

* run bundle exec sidekiq

* it is included a mini test suite containing request specs to make sure things are running
to run it ```bundle exec rspec```



