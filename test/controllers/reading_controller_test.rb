require 'test_helper'

class ReadingControllerTest < ActionDispatch::IntegrationTest
  test "should get get_reading" do
    get reading_get_reading_url
    assert_response :success
  end

  test "should get get_stats" do
    get reading_get_stats_url
    assert_response :success
  end

  test "should get create_reading" do
    get reading_create_reading_url
    assert_response :success
  end

end
