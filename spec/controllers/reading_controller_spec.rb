require 'rails_helper'

describe ReadingController do

  let(:thermostat) { build_stubbed(:thermostat) }

  describe 'post #create_reading', type: :controller do

    it "return 401 when token is not initialized" do
      post :create_reading, params: {temperature: 1.1, battery_charge: 1.1, humidity: 1.1}
      expect(response.status).to match(401)
    end

    it "return tracking number starting with 1 when create new reading" do
      thermostat_x = create(:thermostat, household_token: "123123123")
      request.headers["AUTHORIZATION"] = thermostat_x.household_token
      post :create_reading, params: {temperature: 1.1, battery_charge: 1.1, humidity: 1.1}
      response_body = JSON.parse(response.body)
      expect(response_body["tracking_number"]).to eq 1
    end

    it "tracking number is incremented every reading created" do
      thermostat_x = create(:thermostat, household_token: "123123123")
      request.headers["AUTHORIZATION"] = thermostat_x.household_token
      post :create_reading, params: {temperature: 1.1, battery_charge: 1.1, humidity: 1.1}
      post :create_reading, params: {temperature: 1.1, battery_charge: 1.1, humidity: 1.1}
      response_body = JSON.parse(response.body)
      expect(response_body["tracking_number"]).to eq 2
    end



  end

  describe 'get #get_reading', type: :controller do

    it "get reading even if data not written to db" do
      # initialize data
      thermostat_x = create(:thermostat, household_token: "123123123")
      request.headers["AUTHORIZATION"] = thermostat_x.household_token
      post :create_reading, params: {temperature: 1.1, battery_charge: 1.1, humidity: 1.1}

      # test case
      get :get_reading, params: {tracking_number: 1}
      response_body = JSON.parse(response.body)
      expect(response_body["temperature"]).to eq 1.1
    end


  end


  describe 'get #get_stats', type: :controller do

    it "get stats even if data not written to db" do
      # initialize data
      thermostat_x = create(:thermostat, household_token: "123123123")
      request.headers["AUTHORIZATION"] = thermostat_x.household_token
      post :create_reading, params: {temperature: 1, battery_charge: 1, humidity: 1}
      post :create_reading, params: {temperature: 2, battery_charge: 2, humidity: 2}

      # test case
      get :get_stats
      response_body = JSON.parse(response.body)
      expect(response_body["temperature"]["min"]).to eq 1.0
      expect(response_body["temperature"]["max"]).to eq 2.0
      expect(response_body["temperature"]["average"]).to eq 1.5
    end

    it "returns nil if there is no reading" do
      thermostat_x = create(:thermostat, household_token: "123123123")
      request.headers["AUTHORIZATION"] = thermostat_x.household_token
      get :get_stats
      expect(response.status).to eq(200)
      response_body = JSON.parse(response.body)
      expect(response_body["temperature"]["average"]).to be_nil
      expect(response_body["temperature"]["min"]).to be_nil
      expect(response_body["temperature"]["max"]).to be_nil
    end


  end



end