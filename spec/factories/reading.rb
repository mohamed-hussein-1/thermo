FactoryBot.define do
  factory :reading do
    temperature {1.0}
    humidity {1.0}
    battery_charge {1.0}
    thermostat
  end
end